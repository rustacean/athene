mod controller;
mod middleware;
mod util;

use proc_macro::TokenStream;
use syn::{parse_macro_input, AttributeArgs, ItemImpl};

#[allow(dead_code)]
#[proc_macro_attribute]
pub fn controller(args: TokenStream, input: TokenStream) -> TokenStream {
    let args = parse_macro_input!(args as AttributeArgs);
    let input = parse_macro_input!(input as ItemImpl);

    let expanded =
        controller::expand_controller(args, input).unwrap_or_else(|e| e.to_compile_error());

    TokenStream::from(expanded)
}

#[proc_macro_attribute]
pub fn middleware(_args: TokenStream, input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as ItemImpl);

    let expanded = middleware::expand_middleware(input).unwrap_or_else(|e| e.to_compile_error());

    TokenStream::from(expanded)
}
