mod body;
mod context;
mod controller;
#[cfg(feature = "download")]
mod download;
mod error;
mod fs;
mod handler;
mod middleware;
#[cfg(feature = "multipart")]
mod multipart;
mod request;
mod resolver;
mod responder;
mod response;
mod router;
#[cfg(feature = "https")]
mod rustls;
mod server;
#[cfg(feature = "static_file")]
mod static_dir;
mod tokio_io;
#[cfg(feature = "websocket")]
mod ws;

pub use async_trait;
pub use futures;
pub use headers;
pub use regex;
pub use serde_json;
pub use serde_urlencoded;
pub use thiserror;

pub use fs::{NamedFile, NamedFileBuilder};

pub type Result<T = context::Context, E = error::Error> = std::result::Result<T, E>;
pub type Router<'a> = router::RouterBuilder<'a, router::HandlerChain>;
pub mod prelude {
    #[cfg(feature = "download")]
    pub use crate::download::DispositionType;
    #[cfg(feature = "https")]
    pub use crate::rustls::{ClientAuth, TlsConfig};
    #[cfg(feature = "static_file")]
    pub use crate::static_dir::StaticDir;
    #[cfg(feature = "websocket")]
    pub use crate::ws::WebSocket;
    pub use crate::{
        context::Context,
        controller::{Controller, ControllerBuilder, ControllerMethod},
        error::Error,
        middleware::{Middleware, Next},
        request::{Request, RequestExt},
        responder::{Form, Html, Json, Query, Responder},
        response::Builder,
        Result, Router,
    };
    pub use athene_macro::{controller, middleware};
    #[cfg(feature = "cookie")]
    pub use cookie::{Cookie, CookieJar};
    pub use hyper::StatusCode;
    pub use serde_json::json;
    #[cfg(feature = "websocket")]
    pub use tokio_tungstenite::tungstenite::Message;
    #[cfg(feature = "validate")]
    pub use validator::Validate;
}

#[must_use = "Please use this function to create an HTTP service!"]
#[inline]
pub fn new<'a>() -> server::Builder<'a, crate::router::HandlerChain, crate::middleware::ImplNext> {
    crate::server::Builder::<'a, crate::router::HandlerChain, crate::middleware::ImplNext>::new()
}
