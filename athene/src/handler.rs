use crate::{request::Request, responder::Responder, response::Builder};
use async_trait::async_trait;
use std::future::Future;

#[async_trait]
pub trait Handler: Send + Sync + 'static {
    async fn handle(&self, req: Request) -> Builder;
}

#[async_trait]
impl<R, Fut, F> Handler for F
where
    R: Responder + 'static + Send,
    Fut: Future<Output = R> + Send + 'static,
    F: Fn(Request) -> Fut + Sync + Send + 'static,
{
    #[inline]
    async fn handle(&self, req: Request) -> Builder {
        let builder = Builder::new();
        self(req).await.response(builder)
    }
}
