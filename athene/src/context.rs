use crate::{request::Request, response::Response, router::Router,error::Error};

#[derive(Default)]
pub enum State {
    Before(Box<Request>),
    After(Box<Response>),
    #[default]
    Empty,
}

impl State {
    /// Take the current request leaving `State::Empty` behind
    /// Returns `Some(Request)` if the state was `Before` or `None` if it was
    /// something else
    #[inline]
    pub fn take_request(&mut self) -> Result<Request,Error> {
        match std::mem::take(self) {
            State::Before(r) => Ok(*r),
            _ => Err(Error::Other(String::from("Each middleware must return a context instance"))),
        }
    }

    /// Take the current response leaving `State::Empty` behind
    /// Returns `Some(Response)` if the state was `After` or `None` if it was
    /// something else
    #[inline]
    pub fn take_response(&mut self) -> Result<Response,Error> {
        match std::mem::take(self) {
            State::After(r) => Ok(*r),
            _ => Err(Error::Other(String::from("Each middleware must return a context instance"))),
        }
    }

    /// Returns `Some` of the current request if state if `Before`
    #[inline]
    pub fn request(&self) -> Result<&Request,Error> {
        match self {
            State::Before(r) => Ok(r),
            _ => Err(Error::Other(String::from("Each middleware must return a context instance"))),
        }
    }

    /// Returns `Some` of the current response if state if `After`
    #[inline]
    pub fn response(&self) -> Result<&Response,Error> {
        match self {
            State::After(r) => Ok(r),
            _ => Err(Error::Other(String::from("Each middleware must return a context instance"))),
        }
    }

    /// Returns `Some` of the current request as a mutable ref if state if `Before`
    #[inline]
    pub fn request_mut(&mut self) -> Result<&mut Request,Error> {
        match self {
            State::Before(r) => Ok(r),
            _ => Err(Error::Other(String::from("Each middleware must return a context instance"))),
        }
    }

    /// Returns `Some` of the current response as a mutable reference if state if `After`
    #[inline]
    pub fn response_mut(&mut self) -> Result<&mut Response,Error> {
        match self {
            State::After(r) => Ok(r),
            _ => Err(Error::Other(String::from("Each middleware must return a context instance"))),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum RouteId {
    Id(u64),
    Error(u16),
}

impl RouteId {
    #[inline]
    pub(crate) fn new(id: u64) -> Self {
        RouteId::Id(id)
    }
}

impl Default for RouteId {
    #[inline]
    fn default() -> Self {
        RouteId::Error(404)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Default)]
pub struct HandlerMetadata {
    pub route_id: RouteId,
    pub name: Option<&'static str>,
}

impl HandlerMetadata {
    #[inline]
    pub(crate) fn not_found() -> Self {
        HandlerMetadata {
            route_id: Default::default(),
            name: None,
        }
    }
    #[inline]
    pub(crate) fn not_allowed() -> Self {
        HandlerMetadata {
            route_id: RouteId::Error(405),
            name: None,
        }
    }
}

pub struct Context {
    pub state: State,
    pub metadata: HandlerMetadata,
    pub(crate) router: Option<Router>,
}

impl Context {
    #[inline]
    pub(crate) fn new(request: Request, router: Router, metadata: HandlerMetadata) -> Self {
        let state = State::Before(Box::new(request));
        let router = Some(router);
        Self {
            state,
            metadata,
            router,
        }
    }

    /// Explicitly set the inner state to `Before` with the given response
    #[inline]
    pub fn before(&mut self, request: Request) {
        self.state = State::Before(Box::new(request))
    }

    /// Explicitly set the inner state to `After` with the given response
    #[inline]
    pub fn after(&mut self, response: Response) {
        self.state = State::After(Box::new(response))
    }
}
