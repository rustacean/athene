#[cfg(feature = "multipart")]
use crate::multipart::FilePart;
use crate::{
    body::HttpBody,
    error::Error,
    responder::{Form, FromBytes, Json, Query},
};
use async_trait::async_trait;
#[cfg(feature = "cookie")]
use cookie::{Cookie, CookieJar};
use headers::{Header, HeaderMapExt};
use http_body_util::BodyExt;
use hyper::body::Bytes;
#[cfg(feature = "multipart")]
use multimap::MultiMap;

use serde::de::DeserializeOwned;
use std::{collections::HashMap, net::SocketAddr};

pub type Request<T = HttpBody> = hyper::http::Request<T>;

#[async_trait]
pub trait RequestExt: Sized {
    #[cfg(feature = "cookie")]
    fn cookie(&mut self, name: &str) -> Option<Cookie<'static>>;

    #[cfg(feature = "cookie")]
    fn cookies(&mut self) -> CookieJar;

    #[cfg(feature = "static_file")]
    fn accept(&self) -> Vec<mime::Mime>;

    #[cfg(feature = "static_file")]
    fn first_accept(&self) -> Option<mime::Mime>;

    fn header<T: Header>(&self) -> Option<T>;

    fn remote_addr(&self) -> Result<&SocketAddr, Error>;

    fn params(&self) -> &HashMap<String, String>;

    fn params_mut(&mut self) -> &mut HashMap<String, String>;

    fn param<T: std::str::FromStr>(&mut self, key: &str) -> Result<T, Error>;

    fn query<'de, B>(&'de self) -> Result<B, Error>
    where
        B: serde::Deserialize<'de>;

    fn content_type(&self) -> Option<&str>;

    #[cfg(feature = "multipart")]
    async fn file_part(&mut self) -> Result<MultiMap<String, FilePart>, Error>;

    #[cfg(feature = "multipart")]
    async fn file(&mut self, key: &str) -> Result<FilePart, Error>;

    #[cfg(feature = "multipart")]
    async fn files(&mut self, key: &str) -> Result<Vec<FilePart>, Error>;

    #[cfg(feature = "multipart")]
    async fn upload(&mut self, key: &str, save_path: &str) -> Result<u64, Error>;

    #[cfg(feature = "multipart")]
    async fn uploads(&mut self, key: &str, save_path: &str) -> Result<String, Error>;

    async fn parse<T>(&mut self) -> Result<T, Error>
    where
        T: DeserializeOwned;

    async fn parse_body<T: FromBytes>(&mut self) -> Result<T::Output, Error>;

    async fn parse_query<T: FromBytes>(&mut self) -> Result<Query<T::Output>, Error>;

    async fn parse_json<T: FromBytes>(&mut self) -> Result<Json<T::Output>, Error>;

    async fn parse_form<T: FromBytes>(&mut self) -> Result<Form<T::Output>, Error>;
}

#[async_trait]
impl RequestExt for Request {
    /// Get `Cookie` from Cookie header
    #[cfg(feature = "cookie")]
    #[inline]
    fn cookie(&mut self, name: &str) -> Option<Cookie<'static>> {
        if let Some(mut cookie_iter) = self
            .headers()
            .get("Cookie")
            .and_then(|cookies| cookies.to_str().ok())
            .map(|cookies_str| cookies_str.split(';').map(|s| s.trim()))
            .map(|cookie_iter| {
                cookie_iter.filter_map(|cookie_s| Cookie::parse(cookie_s.to_string()).ok())
            })
        {
            cookie_iter.find(|cookie| cookie.name() == name)
        } else {
            None
        }
    }

    /// Get Cookies from the Cookie header
    #[cfg(feature = "cookie")]
    #[inline]
    fn cookies(&mut self) -> CookieJar {
        let mut jar = CookieJar::new();
        if let Some(cookie_iter) = self
            .headers()
            .get("Cookie")
            .and_then(|cookies| cookies.to_str().ok())
            .map(|cookies_str| cookies_str.split(';').map(|s| s.trim()))
            .map(|cookie_iter| {
                cookie_iter.filter_map(|cookie_s| Cookie::parse(cookie_s.to_string()).ok())
            })
        {
            cookie_iter.for_each(|c| jar.add_original(c))
        }
        jar
    }

    /// Get accept.
    #[cfg(feature = "static_file")]
    #[inline]
    fn accept(&self) -> Vec<mime::Mime> {
        let mut list: Vec<mime::Mime> = vec![];
        if let Some(accept) = self.headers().get("accept").and_then(|h| h.to_str().ok()) {
            let parts: Vec<&str> = accept.split(',').collect();
            for part in parts {
                if let Ok(mt) = part.parse() {
                    list.push(mt);
                }
            }
        }
        list
    }

    /// Get first accept.
    #[cfg(feature = "static_file")]
    #[inline]
    fn first_accept(&self) -> Option<mime::Mime> {
        let mut accept = self.accept();
        if !accept.is_empty() {
            Some(accept.remove(0))
        } else {
            None
        }
    }

    #[inline]
    fn header<T: Header>(&self) -> Option<T> {
        self.headers().typed_get()
    }
    // peer_addr
    #[inline]
    fn remote_addr(&self) -> Result<&SocketAddr, Error> {
        self.extensions()
            .get()
            .ok_or(Error::Other(String::from("Not Found Remote Address")))
    }

    #[inline]
    fn params(&self) -> &HashMap<String, String> {
        self.extensions().get().unwrap()
    }

    #[inline]
    fn params_mut(&mut self) -> &mut HashMap<String, String> {
        self.extensions_mut().get_mut().unwrap()
    }

    #[inline]
    fn param<T: std::str::FromStr>(&mut self, key: &str) -> Result<T, Error> {
        let value = self
            .params_mut()
            .remove(key)
            .ok_or_else(|| Error::MissingParameter(key.to_string(), false))?;
        Ok(value
            .parse::<T>()
            .map_err(|_| Error::InvalidParameter(key.to_string(), false))?)
    }

    #[inline]
    fn query<'de, B>(&'de self) -> Result<B, Error>
    where
        B: serde::Deserialize<'de>,
    {
        let query = self.uri().query().unwrap_or("");
        serde_urlencoded::from_str::<B>(query).map_err(Error::SerdeUrlDe)
    }

    #[inline]
    fn content_type(&self) -> Option<&str> {
        let content_type = self.headers().get("content-type")?;
        let content_type = content_type.to_str().ok()?;
        Some(content_type)
    }

    #[cfg(feature = "multipart")]
    #[inline]
    async fn file_part(&mut self) -> Result<MultiMap<String, FilePart>, Error> {
        let c_type = self.content_type().expect("bad request");
        let boundary = multer::parse_boundary(c_type)?;
        let mut multipart = multer::Multipart::new(std::mem::take(self.body_mut()), &boundary);
        let mut file_parts = MultiMap::new();
        while let Some(mut field) = multipart.next_field().await? {
            if let Some(name) = field.name().map(|s| s.to_owned()) {
                if field.headers().get("content-type").is_some() {
                    file_parts.insert(name, FilePart::new(&mut field).await?);
                }
            }
        }
        Ok(file_parts)
    }

    #[cfg(feature = "multipart")]
    #[inline]
    async fn file(&mut self, key: &str) -> Result<FilePart, Error> {
        let file_part = self.file_part().await?;
        let file_part = file_part.get(key).unwrap();
        Ok(file_part.to_owned())
    }

    #[cfg(feature = "multipart")]
    #[inline]
    async fn files(&mut self, key: &str) -> Result<Vec<FilePart>, Error> {
        let file_part = self.file_part().await?;
        let file_part = file_part.get_vec(key).unwrap();
        Ok(file_part.to_owned())
    }

    #[cfg(feature = "multipart")]
    #[inline]
    async fn upload(&mut self, key: &str, save_path: &str) -> Result<u64, Error> {
        let file = self.file(key).await?;
        std::fs::create_dir_all(save_path)?;
        let dest = format!("{}/{}", save_path, file.name().unwrap());
        Ok(std::fs::copy(file.path(), std::path::Path::new(&dest))?)
    }

    #[cfg(feature = "multipart")]
    #[inline]
    async fn uploads(&mut self, key: &str, save_path: &str) -> Result<String, Error> {
        let files = self.files(key).await?;
        std::fs::create_dir_all(save_path)?;
        let mut msgs = Vec::with_capacity(files.len());
        for file in files {
            let dest = format!("{}/{}", save_path, file.name().unwrap());
            if let Err(e) = std::fs::copy(file.path(), std::path::Path::new(&dest)) {
                return Ok(format!("file not found in request: {e}"));
            } else {
                msgs.push(dest);
            }
        }
        Ok(format!("Files uploaded:\n\n{}", msgs.join("\n")))
    }

    #[inline]
    async fn parse<T>(&mut self) -> Result<T, Error>
    where
        T: DeserializeOwned,
    {
        let body = self.body_mut().collect().await?.to_bytes();
        let essence = self.content_type();
        match essence {
            Some("application/json") => serde_json::from_slice(&body).map_err(Error::SerdeJson),
            Some("application/x-www-form-urlencoded") => {
                serde_urlencoded::from_bytes(&body).map_err(Error::SerdeUrlDe)
            }
            #[cfg(feature = "cbor")]
            Some("application/cbor") => {
                ciborium::de::from_reader(&body[..]).map_err(|e| Error::Other(e.to_string()))
            }
            #[cfg(feature = "msgpack")]
            Some("application/msgpack") => {
                rmp_serde::from_slice(&body).map_err(Error::MsgpackDeserialization)
            }
            _ => Err(Error::Other(String::from("Invalid Context-Type"))),
        }
    }

    #[inline]
    async fn parse_body<T: FromBytes>(&mut self) -> Result<T::Output, Error> {
        let bytes = self.body_mut().collect().await?.to_bytes();
        let value = T::from_bytes(bytes)?;
        Ok(value)
    }

    #[inline]
    async fn parse_query<T: FromBytes>(&mut self) -> Result<Query<T::Output>, Error> {
        let query = String::from(self.uri().query().unwrap_or(""));
        let bytes = Bytes::from(query);
        let value = T::from_bytes(bytes)?;
        Ok(Query(value))
    }

    #[inline]
    async fn parse_json<T: FromBytes>(&mut self) -> Result<Json<T::Output>, Error> {
        let bytes = self.body_mut().collect().await?.to_bytes();
        let value = T::from_bytes(bytes)?;
        Ok(Json(value))
    }

    #[inline]
    async fn parse_form<T: FromBytes>(&mut self) -> Result<Form<T::Output>, Error> {
        let bytes = self.body_mut().collect().await?.to_bytes();
        let value = T::from_bytes(bytes)?;
        Ok(Form(value))
    }
}
