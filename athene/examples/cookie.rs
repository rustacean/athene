use athene::prelude::*;

pub async fn get_cookies(mut req: Request) -> impl Responder {
    let cookie = req.cookie("test1").unwrap();
    let res = Builder::new();
    res.status(StatusCode::OK).text(format!("cookie_name = {:?}",cookie.name()))
}
pub async fn set_cookies(_req: Request) -> impl Responder {
    let mut cookie = Cookie::new("test1", "athene");
    cookie.set_path("/");

    let mut cookie2 = Cookie::new("test2", "athene2");
    cookie2.set_path("/");

    let mut cookie_jar = CookieJar::new();

    cookie_jar.add(cookie);
    cookie_jar.add(cookie2);

    (200, cookie_jar)
}

#[tokio::main]
async fn main() -> Result<()> {
    let app = athene::new();

    let app = app.router(|r|
        r.get("/set_cookies", set_cookies)
        .get("/get_cookies", get_cookies)
    );
    
    app.listen("127.0.0.1:7878").await
}