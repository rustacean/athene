use athene::prelude::*;

pub async fn download(_req: Request) -> impl Responder {
    let res = Builder::new();
    res.write_file("Cargo.toml", DispositionType::Attachment)
}

// 127.0.0.1:7878/download
#[tokio::main]
pub async fn main() -> Result<()> {
    let app = athene::new();
    let app = app.router(|r| r.get("/download", download));
    app.listen("127.0.0.1:7878").await
}
