use athene::prelude::*;

#[tokio::main]
async fn main() -> Result<()> {
    let app =
        athene::new().router(|r| r.get("/**", StaticDir::new("athene").with_listing(true)));
    app.listen("127.0.0.1:7878").await
}
