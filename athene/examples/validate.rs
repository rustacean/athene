use athene::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Validate, Default)]
pub struct UserController {
    #[validate(email)]
    pub email: String, // admin@outlook.com
    #[validate(range(min = 18, max = 20))]
    pub age: u16,
}

// http://127.0.0.1:7878/api/v1/user
#[controller(prefix = "api", version = 1, name = "user")]
impl UserController {
    // http://127.0.0.1:7878/api/v1/user/query/?email=admin@outlook.com&age=19
    // query params
    #[get("/query")] // user will be validate
    pub async fn query(&self, user: Query<Self>) -> impl Responder {
        (200, Json(user.0))
    }

    // http://127.0.0.1:7878/api/v1/user/create
    // Context-Type : application/json
    #[post("/create")] // user will be validate
    async fn create(&self, user: Json<Self>) -> impl Responder {
        Ok::<_, Error>((200, user))
    }

    // http://127.0.0.1:7878/api/v1/user/update
    // Context-Type : application/x-www-form-urlencoded
    #[put("/update")]
    #[validator(exclude("user"))] // user will not be validate
    async fn update(&self, user: Form<Self>) -> impl Responder {
        Ok::<_, Error>((200, user))
    } 

    // http://127.0.0.1:7878/api/v1/user/custom_validate
    // Context-Type : application/json
    #[post("/custom_validate")]
    async fn custom_validate(&self,mut req: Request) -> impl Responder {
        let user = req.parse::<Self>().await?;
        user.validate()?;
       Ok::<_, Error>((200,Json(user)))
    }
}

#[tokio::main]
pub async fn main() -> Result<()> {
    tracing_subscriber::fmt().compact().init();

    let app = athene::new().router(|r| {
        r.controller(UserController::default())
    });
    app.listen("127.0.0.1:7878").await
}