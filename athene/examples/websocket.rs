use athene::prelude::*;

// Websocket  ws://127.0.0.1:7878/hello/ws  http://www.jsons.cn/websocket/
pub async fn ws(
    mut req: Request,
    mut ws: WebSocket,
) -> Result<()> {
    let name = req.param::<String>("name")?;
    while let Ok(msg) = ws.receive().await {
        if let Some(msg) = msg {
            match msg {
                Message::Text(text) => {
                    let text = format!("{},{}", name, text);
                    ws.send(Message::Text(text)).await?;
                }
                Message::Close(_) => break,
                _ => {}
            }
        }
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    
    let app = athene::new();

    let app = app.router(|r|r.ws("/{name}/ws", ws));

    app.listen("127.0.0.1:7878").await
}