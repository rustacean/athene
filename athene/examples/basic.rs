use athene::prelude::*;

pub async fn delete_by_name(mut req: Request) -> impl Responder {
    let name = req.param::<String>("name")?;
    /*
        .....
     */
    Ok::<_,Error>((StatusCode::OK,name))
}

pub fn basic_router(r: Router) -> Router {
    r.delete("/{name}", delete_by_name)
     .get("/hello",|_|async {
            "Hello"
        })
     .get("/redirect_location",|_|async {
            "Redirect location"
    })
}

#[tokio::main]
async fn main() -> Result<()> {
    let app = athene::new();

    let app = app
    .router(basic_router)
    .router(|r|{

        // Remote Address
        r.get("/remote_addr",|req: Request| async move {
            let remote_addr = req.remote_addr()?;
            Ok::<_,Error>((StatusCode::OK,remote_addr.to_string()))
        })

        // Permanent Redirect
        .get("/permanent", |_| async {
            let res = Builder::new();
            res.redirect(StatusCode::PERMANENT_REDIRECT,"/remote_addr")
        })

        // Temporary Redirect
        .get("/temporary", |_| async {
            let res = Builder::new();
            res.redirect(StatusCode::TEMPORARY_REDIRECT,"/hello")
        })
    });

    app.listen("127.0.0.1:7878").await
}